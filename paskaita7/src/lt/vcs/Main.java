/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import static lt.vcs.VcsUtils.*;


/**
 *
 * @author sigitasbeleckas
 */
public class Main {
    
    private static final String PVZ_FAILAS = "/Users/sigitasbeleckas/Temp/pvz.txt";
    private static final String TEMPT = "/Users/sigitasbeleckas/Temp/";
    private static final String UTF_8 = "UTF-8";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        File pvzFile = new File(PVZ_FAILAS);
        BufferedReader br = null;
        String failoTekstas = "";
        String line;
        try {
            br = new BufferedReader(
                new InputStreamReader(new FileInputStream(pvzFile),
                        UTF_8));
                while ((line = br.readLine()) != null) {
                    failoTekstas += line + System.lineSeparator();
                    out(line);
                    
                }
        } catch (Exception e) {
        out(e.getMessage());
        } finally {
            if (br != null) { 
                try {
                    br.close();
                } catch (Exception e){
                    out(e.getMessage());
                }
                
            }
        }
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(pvzFile),
                        UTF_8));
            bw.append(failoTekstas);
            bw.append("sessioliks 16 eilute");
            bw.flush();
            
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (br != null) { 
                try {
                    br.close();
                } catch (Exception e){
                    out(e.getMessage());
                }
             
        }
        }
    }
        
    private static File newFile(String dir, String fileName) {
        File result = null;
        
        //File pvzDir = new File(dir);
        //File pvzName = new File(fileName);
        
        if (dir != null && fileName != null) {
           File pvzDir = new File(dir);
           if (pvzDir.isDirectory()) {
               if (pvzDir.exists()) {
                   
                   try {
                   result = new File(pvzDir, fileName);    
                   result.createNewFile();
                   } catch (Exception e) {
                       out(e.getMessage());
                   }
                   
               } else {
                   try {
                       pvzDir.mkdirs();
                   } catch (Exception e) {
                       out(e.getMessage());
                   }
                           
               }
           }else {
               out("tai ne direktorija");
           }
          
        }
         
        return result;
    }   
    
    
}
