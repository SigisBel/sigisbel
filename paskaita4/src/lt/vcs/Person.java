/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author sigitasbeleckas
 */
public class Person {
    
    private String name;
    private String surname;
    protected Gender gender;
    private int age;

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }
    
    public Person(String name) {
        this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
    }
    
    public Person(String name, Gender gender) {
        this(name);
        this.gender = gender;
    }
    public Person(String name, String surname, Gender gender, int age) {
        this(name, gender);
        this.surname = surname;
        this.age = age;
    }
    
    @Override
    public String toString() {
        return getClass().getSimpleName() + " (name = " + getName() + " gender " + gender.getEnLabel() + " )";
        
    }
    
    
}
