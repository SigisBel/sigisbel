/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;



import static lt.vcs.VcsUtils.*;
/**
 *
 * @author sigitasbeleckas
 */
public class Player extends Person {
    
    private int dice;

    /*public Player(String name, Gender gender) {
        this(name);
        
        this.gender = gender;
    }
    
    public Player(String name) {
        this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        
    }*/
    
    public Player(String name) {
        super(name);
        this.dice = random(1, 6);
    }
    
    public Player(String name, Gender gender) {
        this(name);
        this.gender = gender;
        this.dice = random(1, 6);
    }
    
    public Player(String name, String surname, Gender gender, int age) {
        this(name, gender);
        setSurname(surname);
        setAge(age);
        
    }
    
    @Override
    public String toString() {
        return super.toString().replaceFirst("\\)", " dice=" + dice + ")");
       
    }

    public int getDice() {
        return dice;
    }
    
}
