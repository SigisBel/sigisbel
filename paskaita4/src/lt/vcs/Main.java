/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.List;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author sigitasbeleckas
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Player p1 = new Player(inWord("Iveskite Varda: "), Gender.getByld(inInt("Iveskite lyti: ")));
        Person per1 = new Person("zmogenas");
        per1 = p1;
        
        Object obj = p1;
        out(p1);
        
        
        User<Person> u1 = new User(per1);
        User<Player> u2 = new User(p1);
        
        out(u1.getPerson());
        out(u2.getPerson());
        
        List<User> lst = new ArrayList();
        lst.add(u2);
        lst.add(u1);
        
        
    }
    
}
