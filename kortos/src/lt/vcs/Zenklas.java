/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author sigitasbeleckas
 */
public enum Zenklas {
    K("kryziai"),
    V("vynai"),
    B("bugnai"),
    S("sirdys");
    
    
    private String zenklas;

    public String getZenklas() {
        return zenklas;
    }
    
    private Zenklas(String zenklas) {
        this.zenklas = zenklas;
    } 
}





